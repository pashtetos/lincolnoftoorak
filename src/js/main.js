//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/swiper/dist/js/swiper.jquery.js

var swiper = new Swiper('.bg-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    loop: true,
    effect: 'fade',
    autoplay: 4000
});


var map;
function initMap() {
    var myLatLng = {lat: 50.3954337, lng: 30.6338324};

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 50.3954337, lng: 30.6338324},
        zoom: 16,
        styles: [
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
        ],
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        icon: '../img/map-marker.png',
        map: map
    });
}


$(document).ready(function () {
    var touch = $('.burger');
    var menu = $('.navigation');

    $(touch).on('click', function (e) {
        e.preventDefault();
        menu.slideToggle();
    });
    $(window).resize(function () {
        var wid = $(window).width();
        if (wid > 760 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
    initMap();

        $(window).scroll(function () {
            if (($(this).scrollTop() > 50) && ($(window).width() > 992)) {
                $('header').addClass("sticky");
            }
            else {
                $('header').removeClass("sticky");
            }
        })


if ($(window).width() < 1200) {
    var swiper = new Swiper('.swiper-container', {
        // loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        nextButton: '.swiper-btn-next',
        prevButton: '.swiper-btn-prev',
        breakpoints: {
            1200: {
                slidesPerView: 'auto',
                spaceBetween: 20,
                nextButton: '.swiper-btn-next',
                prevButton: '.swiper-btn-prev'
            },
            992: {
                slidesPerView: "auto",
                spaceBetween: 20,
                nextButton: '.swiper-btn-next',
                prevButton: '.swiper-btn-prev'
            },
            320: {
                slidesPerView: "1",
                spaceBetween: 15,
                nextButton: '.swiper-btn-next',
                prevButton: '.swiper-btn-prev'
            }
        }
    });
}


$('a#go').click(function (event) {
    event.preventDefault();
    $('body').css('overflow-y', 'hidden');
    $('#overlay').fadeIn(400,
        function () {
            $('#modal_form')
                .css('display', 'block')
                .animate({opacity: 1, top: '50%'}, 200);
        });
});
$('#modal_close').click(function () {
    $('body').css('overflow-y', 'scroll');
    $('#modal_form')
        .animate({opacity: 0, top: '45%'}, 200,
            function () {
                $('body').css('overflow-y', 'scroll');
                $('#modal_form').css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
});
modal = $('#overlay');
$('body').css('overflow-y', 'scroll');
$(window).on('click', function (event) {
    if (event.target == modal[0]) {
        $('body').css('overflow-y', 'scroll');
        modal.css('display', 'none');
        $('#overlay').fadeOut(400);
    }
});


})
;

// jQuery("document").ready(function ($) {
//     $(window).scroll(function () {
//         if ($(this).scrollTop() > 114) {
//             $("#header").show()
//         }
//         else {
//             $("#header").hide()
//         }
//     })
// });






